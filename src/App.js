import React from 'react';
import Camera from './components/camera';

function App() {
  return (
    <div>
      <Camera />
    </div>
  );
}

export default App;
