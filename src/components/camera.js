import React, { Component } from 'react';
import Webcam from 'react-webcam';
import './camera.css';

const videoConstraints = {
  width: 750,
  height: 1334,
  facingMode: { exact: "environment" }
};

const Camera = () => {
  const webcamRef = React.useRef(null);

  const capture = React.useCallback(
    () => {
      const imageSrc = webcamRef.current.getScreenshot();
    },
    [webcamRef]
  );

  return (
    <>
      <div className='container'>
        <Webcam
          audio={false}
          height={750}
          width={1334}
          ref={webcamRef}
          screenshotFormat="image/jpeg"
          videoConstraints={videoConstraints}
        />
      </div>
      <button onClick={capture}>Capture photo</button>
    </>
  );
};

// const videoConstraints = {
//   width: 1280,
//   height: 720,
//   facingMode: "user"
// };
 
// const Camera = () => {
//   const webcamRef = React.useRef(null);
 
  // const capture = React.useCallback(
  //   () => {
  //     console.log('pressed');
  //     const imageSrc = webcamRef.current.getScreenshot();
  //     console.log(webcamRef)
  //   },
  //   [webcamRef]
  // );
  
//   const capture = () => {
//     webcamRef.current.getScreenshot();
//   }
 
//   return (
//     <div>
//       <div className='container'>
//         <Webcam
//           audio={false}
//           height={720}
//           ref={webcamRef}
//           screenshotFormat="image/jpeg"
//           width={1280}
//           videoConstraints={videoConstraints}
//         />
//       </div>
//       <button onClick={capture}>Capture photo</button>
//     </div>
//   );
// };

export default Camera;
