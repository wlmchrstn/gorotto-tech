/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
let mobilenet;
let classifier;
let video;
let label='need to train';
let model1Button;
let model2Button;
let trainButton;
let saveButton;

function modelReady() {
	console.log('Model is ready!!!');
}

function videoReady() {
	console.log('Video is ready!!!');
}

function whileTraining(loss) {
	if (loss == null) {
		console.log('Training Complete');
		classifier.classify(gotResults);
	} else {
		console.log(loss);
	}
}

function gotResults(error, result) {
	if (error) {
		console.log(error)
	} else {
		label = result[0].label;
		console.log(label);
		classifier.classify(gotResults);
	}
}

function setup() {
	createCanvas(640, 540);
	video = createCapture(VIDEO);
	video.hide();
	background(0);
	mobilenet = ml5.featureExtractor('MobileNet', modelReady);
	classifier = mobilenet.classification(video, videoReady);
	model1Button = createButton('Model 1');
	model2Button = createButton('Model 2');
	model1Button.mousePressed(function () {
		classifier.addImage('Model 1');
		console.log('Added');
	}); 
	model2Button.mousePressed(function () {
		classifier.addImage('Model 2');
		console.log('Added')
	})
	trainButton = createButton('Train');
	trainButton.mousePressed(function () {
		classifier.train(whileTraining);
	});
	saveButton = createButton('Save');
	saveButton.mousePressed(function () {
		 classifier.save();
	})
}

function draw() {
	background(0);
	image(video, 0,0);
	fill(255);
	textSize(32);
	text(label, 10, height - 20);
}