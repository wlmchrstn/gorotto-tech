/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
let mobilenet;
let classifier;
let video;
let label='loading model';

function modelReady() {
	console.log('Model is ready!!!');
	classifier.load('model.json', customModelReady);
	label = 'model ready';
}

function customModelReady() {
	console.log('Custom Model is ready!!!');
	classifier.classify(gotResults);
}

function videoReady() {
	console.log('Video is ready!!!');
}

function gotResults(error, result) {
	if (error) {
		console.log(error)
	} else {
		label = result[0].label;
		console.log(label);
		classifier.classify(gotResults);
	}
}

function setup() {
	createCanvas(640, 540);
	video = createCapture(VIDEO);
	video.hide();
	background(0);
	mobilenet = ml5.featureExtractor('MobileNet', modelReady);
	classifier = mobilenet.classification(video, videoReady);
}

function draw() {
	background(0);
	image(video, 0,0);
	fill(255);
	textSize(32);
	text(label, 10, height - 20);
}